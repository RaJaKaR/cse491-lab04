package com.example.lab04;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class FetchJSON extends AsyncTask<Void, Void, Void> {

    String fetchedData = "";
    String dataParsed = "";
    String singleParsed = "";

    ArrayList<Employee> employees = new ArrayList<>();

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL jsonUrl = new URL("https://api.myjson.com/bins/1fp9d4");
            HttpURLConnection connection = (HttpURLConnection) jsonUrl.openConnection();
            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String data = "";
            while(data != null) {
                data = bufferedReader.readLine();
                fetchedData = fetchedData + data;
            }

            JSONArray jsonArray = new JSONArray(fetchedData);
            for (int i = 0 ; i < jsonArray.length() ; ++i) {
                JSONObject jo = (JSONObject) jsonArray.get(i);

                if (jo.isNull("location")) {
                    String name = jo.getString("name");
                    employees.add(new Employee(name));
                }
                else {
                    String name = jo.getString("name");
                    double lat = jo.getJSONObject("location").getDouble("latitude");
                    double lng = jo.getJSONObject("location").getDouble("longitude");
                    employees.add(new Employee(name, lat, lng));
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        MainActivity.jsonData = fetchedData;
        MainActivity.employeesList = employees;
    }
}
