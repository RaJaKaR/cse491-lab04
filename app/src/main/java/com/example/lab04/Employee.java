package com.example.lab04;

public class Employee {
    String name;
    double latitude;
    double longitude;

    public Employee (String n, double lat, double lon) {
        name = n;
        latitude = lat;
        longitude = lon;
    }

    public Employee (String n) {
        name = n;
        latitude = 0.0;
        longitude = 0.0;
    }
}
