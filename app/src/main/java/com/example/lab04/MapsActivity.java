package com.example.lab04;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String name = "";
    private double lat = 0.0;
    private double lng = 0.0;
    private String id = "";
    private static final int REQUEST_LOCATION = 123;
    private Marker employeeMarker;
    EmployeDatabaseHelper employeDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Intent iin = getIntent();
        Bundle employeeInfo = iin.getExtras();
        name = (String) employeeInfo.get("name");
        lat = (double) employeeInfo.get("lat");
        lng = (double) employeeInfo.get("lng");
        id = employeeInfo.get("index").toString();

        employeDatabaseHelper = new EmployeDatabaseHelper(this);
        final SQLiteDatabase sqLiteDatabase = employeDatabaseHelper.getWritableDatabase();

        if (lat == 0.0 && lng == 0.0) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,

                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,

                                Manifest.permission.ACCESS_COARSE_LOCATION},REQUEST_LOCATION);
                return;
            }
            LocationManager locationManager = (LocationManager) getSystemService(getBaseContext().LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            lat = location.getLatitude();
            lng  = location.getLongitude();
            saveLocationDialog();
        }

    }

    private void saveLocationDialog() {
        AlertDialog.Builder dialog=new AlertDialog.Builder(this);
        dialog.setMessage("No Location Found. Do you want to save your current location?\n\nLat: " + lat + "\nLng: " + lng);
        dialog.setTitle("Save Location");
        dialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        employeDatabaseHelper.updateData(id, name, lat, lng);
                    }
                });
        dialog.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"Location not saved",Toast.LENGTH_LONG).show();
            }
        });
        AlertDialog alertDialog=dialog.create();
        alertDialog.show();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng employeeLocation = new LatLng(lat, lng);
        employeeMarker = mMap.addMarker(new MarkerOptions().position(employeeLocation).title(name));
        employeeMarker.showInfoWindow();
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                          @Override
                                          public void onInfoWindowClick(Marker m) {
                                              if(m.equals(employeeMarker)) {
                                                  Intent timerIntent = new Intent(getApplicationContext(), Timer.class);
                                                  startActivity(timerIntent);
                                              }
                                          }
                                      });
        mMap.moveCamera(CameraUpdateFactory.newLatLng(employeeLocation));
    }
}
