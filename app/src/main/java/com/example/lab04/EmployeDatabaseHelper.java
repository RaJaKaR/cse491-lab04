package com.example.lab04;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;

public class EmployeDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Emplyee.db";
    private static final String TABLE_NAME = "employee_details";
    private static final String COL_ID = "_id";
    private static final String COL_NAME = "Name";
    private static final String COL_LAT = "Latitude";
    private static final String COL_LNG = "Longitude";
    private static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"("+COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+COL_NAME+" VARCHAR(255),"+COL_LAT+" FLOAT,"+COL_LNG+" FLOAT);";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE_NAME;
    private static final int VERSION_NUMBER = 4;
    private Context context;


    public EmployeDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION_NUMBER);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Toast.makeText(context,"Initializing Database",Toast.LENGTH_SHORT).show();
            db.execSQL(CREATE_TABLE);
        }catch (Exception e){
            Toast.makeText(context,"Create Failed",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try{
            Toast.makeText(context,"onUpgrade is called",Toast.LENGTH_SHORT).show();
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }catch (Exception e) {
            Toast.makeText(context,"Upgrade failed",Toast.LENGTH_SHORT).show();
        }
    }

    public long insertData(String name, double lat, double lng) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME, name);
        contentValues.put(COL_LAT, lat);
        contentValues.put(COL_LNG, lng);
        long rowId = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);
        return rowId;
    }

    public Cursor getData() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(SELECT_ALL, null);
        return cursor;
    }

    public void updateData(String id, String name, double lat, double lng){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_ID, id);
        contentValues.put(COL_NAME, name);
        contentValues.put(COL_LAT, lat);
        contentValues.put(COL_LNG, lng);
        sqLiteDatabase.update(TABLE_NAME, contentValues, COL_ID+" = ?", new String[]{id});

        ArrayList<Employee> newList= new ArrayList<>();
        Cursor resultSet = getData();
        while (resultSet.moveToNext()) {
            String newname = resultSet.getString(1);
            double newlat = resultSet.getFloat(2);
            double newlng = resultSet.getFloat(3);
            newList.add(new Employee(newname, newlat, newlng));
        }
        MainActivity.employeesList = newList;

        Toast.makeText(context,"DB Updated",Toast.LENGTH_LONG).show();
    }
}
