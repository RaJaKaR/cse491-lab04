package com.example.lab04;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static String jsonData = "";
    public static ArrayList<Employee> employeesList = new ArrayList<>();
    public static ArrayList<String> employeeNames;
    public static ArrayAdapter<String> employeesListAdapter;
    public static ListView lvEmployees;
    public Button showDB;
    public String dbHasData = "false";

    EmployeDatabaseHelper employeDatabaseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btJson = findViewById(R.id.btJson);
        Button btDBCreate = findViewById(R.id.btDBCreate);
        showDB = findViewById(R.id.btShowDB);

        lvEmployees = findViewById(R.id.lvEmployees);
        employeeNames = new ArrayList<>();
        employeesListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, employeeNames);
        lvEmployees.setAdapter(employeesListAdapter);
        employeeNames = new ArrayList<>();

        employeDatabaseHelper = new EmployeDatabaseHelper(this);
        final SQLiteDatabase sqLiteDatabase = employeDatabaseHelper.getWritableDatabase();

        readDatabaseStatus();

        btJson.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FetchJSON process = new FetchJSON();
                        process.execute();
                        Toast.makeText(getApplicationContext(), "Fetching Data from json", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        btDBCreate.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (Employee e : employeesList) {
                            long rowId = employeDatabaseHelper.insertData(e.name, e.latitude, e.longitude);
                            if (rowId > 0)
                                Toast.makeText(getApplicationContext(), "DB created with " + rowId + " Items", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        showDB.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor resultSet = employeDatabaseHelper.getData();
                        if (resultSet.getCount() == 0) {
                            Toast.makeText(getApplicationContext(), "DB has no data", Toast.LENGTH_SHORT).show();
                        }
                        while (resultSet.moveToNext()) {
                            String name = resultSet.getString(1);
                            double lat = resultSet.getFloat(2);
                            double lng = resultSet.getFloat(3);
                            employeesList.add(new Employee(name, lat, lng));
                            employeesListAdapter.add(resultSet.getString(1));
                        }
                        saveDatabaseStatus();
                    }
                }
        );

        lvEmployees.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent mapIntent = new Intent(getBaseContext(), MapsActivity.class);
                mapIntent.putExtra("name", employeesList.get(position).name);
                mapIntent.putExtra("lat", employeesList.get(position).latitude);
                mapIntent.putExtra("lng", employeesList.get(position).longitude);
                mapIntent.putExtra("index", position + 1);
                startActivity(mapIntent);
            }
        });
    }

    public void saveDatabaseStatus() {
        File filesDir = getFilesDir();
        File dbFile = new File(filesDir, "db_check.txt");
        try {
            FileUtils.writeStringToFile(dbFile, "true");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readDatabaseStatus() {
        File filesDir = getFilesDir();
        File dbFile = new File(filesDir, "db_check.txt");
        try {
            dbHasData = FileUtils.readFileToString(dbFile);
            if (dbHasData.equals("true")) {
                showDB.post(new Runnable(){
                    @Override
                    public void run() {
                        showDB.performClick();
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
